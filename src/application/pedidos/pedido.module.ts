import { Module } from '@nestjs/common'
import { PedidosController } from './controller'
import { PedidoService } from './service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PedidoRepository } from './repository'
import { ProductosService } from '../productos/service'
import { Pedido } from './entity/pedido.entity'

@Module({
  controllers: [PedidosController],
  providers: [PedidoService, PedidoRepository],
  imports: [TypeOrmModule.forFeature([Pedido])],
  exports: [PedidoService],
})
export class PedidosModule {}
