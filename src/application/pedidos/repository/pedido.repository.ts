import { Injectable } from '@nestjs/common'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'
import { DataSource } from 'typeorm'
import { CrearPedidoDto } from '../dto/crear-pedido.dto'
import { Pedido } from '../entity/pedido.entity'

@Injectable()
export class PedidoRepository {
  constructor(private dataSource: DataSource) {}

  async crear(pedidoDto: CrearPedidoDto, usuarioAuditoria: string) {
    console.log('DATOSPEDIDO:::-->', pedidoDto)
    const { nroPedido, nombre, fechaPedido } = pedidoDto

    const pedido = new Pedido()
    pedido.nroPedido = nroPedido
    pedido.nombre = nombre
    pedido.fechaPedido = fechaPedido
    pedido.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Pedido).save(pedido)
  }
  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Pedido)
      .createQueryBuilder('pedido')
      .select([
        'pedido.id',
        'pedido.nroPedido',
        'pedido.nombre',
        'pedido.fechaPedido',
      ])
      .take(limite)
      .skip(saltar)
    return await query.getManyAndCount()
  }
}
