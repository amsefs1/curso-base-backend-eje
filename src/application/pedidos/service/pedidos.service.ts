import { ProductosService } from '../../../application/productos/service'
import { BaseService } from '../../../common/base/base-service'
import { Injectable, Inject } from '@nestjs/common'
import { CrearPedidoDto } from '../dto/crear-pedido.dto'
import { PedidoRepository } from '../repository/pedido.repository'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'

@Injectable()
export class PedidoService extends BaseService {
  constructor(
    @Inject(PedidoRepository)
    private pedidoRespository: PedidoRepository
  ) {
    super()
  }

  async crear(datos: CrearPedidoDto, usuarioAuditoria: string) {
    const result = await this.pedidoRespository.crear(datos, usuarioAuditoria)
    return result
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    return await this.pedidoRespository.listar(paginacionQueryDto)
  }
}
