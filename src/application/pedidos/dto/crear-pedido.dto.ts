export class CrearPedidoDto {
  nroPedido: string
  nombre: string
  fechaPedido: Date
}
