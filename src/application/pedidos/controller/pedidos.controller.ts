import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common'
import { BaseController } from '../../../common/base'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { PedidoService } from '../service/pedidos.service'
import { CrearPedidoDto } from '../dto/crear-pedido.dto'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'

@Controller('pedidos')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class PedidosController extends BaseController {
  constructor(private pedidosService: PedidoService) {
    super()
  }

  @Post()
  async crear(@Req() req: Request, @Body() datos: CrearPedidoDto) {
    const usuarioAuditoria = '1' //this.getUser(req)
    const result: any = await this.pedidosService.crear(datos, usuarioAuditoria)
    return this.successCreate(result)
  }

  @Get()
  async listar(@Query() PaginacionQueryDto: PaginacionQueryDto) {
    const resultado = await this.pedidosService.listar(PaginacionQueryDto)
    return this.successListRows(resultado)
  }
}
