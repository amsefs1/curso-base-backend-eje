import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common'
import { ProductosService } from '../service'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { BaseController } from '../../../common/base'
import { ParamIdDto } from '../../../common/dto/params-id.dto'
import { Request } from 'express'
import { ActualizarProductoDto, CrearProductoDto } from '../dto'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { ApiOperation, ApiQuery } from '@nestjs/swagger'

@Controller('productos')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class ProductosController extends BaseController {
  constructor(private productosServicio: ProductosService) {
    super()
  }

  @ApiOperation({
    summary:
      'Obtiene productos con posibilidad filtrar por categoria y nombre ',
  })
  @ApiQuery({ name: 'categoria', type: String, required: false })
  @ApiQuery({ name: 'nombre', type: String, required: false })
  @Get()
  async listar(
    @Query() paginacionQueryDto: PaginacionQueryDto,
    @Query('categoria') categoria?: string,
    @Query('nombre') nombre?: string
  ) {
    const result = await this.productosServicio.listar(
      paginacionQueryDto,
      categoria,
      nombre
    )
    return this.successListRows(result)
  }

  @ApiOperation({
    summary: 'Crear producto',
  })
  @Post()
  async crear(@Req() req: Request, @Body() productoDto: CrearProductoDto) {
    // const usuarioAuditoria = this.getUser(req)
    console.log('CONTROLERR---->', productoDto)
    const usuarioAuditoria = '1'
    const result = await this.productosServicio.crear(
      productoDto,
      usuarioAuditoria
    )
    return this.successCreate(result)
  }

  @ApiOperation({
    summary: 'Actualiza un producto según id',
  })
  @Put(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() productoDto: ActualizarProductoDto
  ) {
    const { id: idProducto } = params
    // const usuarioAuditoria = this.getUser(req)
    const usuarioAuditoria = '1'
    const result = await this.productosServicio.actualizarDatos(
      idProducto,
      productoDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }
}
