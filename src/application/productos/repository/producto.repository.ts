import { DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { ActualizarProductoDto, CrearProductoDto } from '../dto'
import { Producto } from '../entity'
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'

@Injectable()
export class ProductoRepository {
  constructor(private dataSource: DataSource) {}

  async crear(productoDto: CrearProductoDto, usuarioAuditoria: string) {
    const { codigo, nombre, categoria, precio } = productoDto

    const producto = new Producto()
    producto.codigo = codigo
    producto.nombre = nombre
    producto.precio = precio
    producto.categoria = categoria
    producto.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Producto).save(producto)
  }

  async listar(
    paginacionQueryDto: PaginacionQueryDto,
    categoria?: string,
    nombre?: string
  ) {
    const { limite, saltar } = paginacionQueryDto
    let query = this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .select([
        'producto.id',
        'producto.codigo',
        'producto.nombre',
        'producto.precio',
        'producto.estado',
        'producto.estado',
        'producto.categoria',
      ])

    if (categoria) {
      query = query.where('producto.categoria = :categoria', { categoria })
    }

    if (nombre) {
      query = query.andWhere('producto.nombre like :nombre', {
        nombre: `%${nombre}%`,
      })
    }
    query = query.take(limite).skip(saltar)

    return await query.getManyAndCount()
  }

  async buscarCodigo(codigo: string) {
    return this.dataSource
      .getRepository(Producto)
      .findOne({ where: { codigo: codigo } })
  }

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ id: id })
      .getOne()
  }

  async actualizar(
    id: string,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string
  ) {
    const { codigo, nombre, precio } = productoDto
    const datosActualizar: QueryDeepPartialEntity<Producto> = new Producto({
      codigo: codigo,
      nombre: nombre,
      precio: precio,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Producto)
      .update(id, datosActualizar)
  }
}
