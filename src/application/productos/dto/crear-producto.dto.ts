import { IsNotEmpty } from '../../../common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearProductoDto {
  @ApiProperty({ example: 'A-001' })
  @IsNotEmpty()
  codigo: string

  @ApiProperty({ example: 'Nombre del producto' })
  @IsNotEmpty()
  nombre: string

  @ApiProperty({ example: 'Nombre de l categoria' })
  @IsNotEmpty()
  categoria: string

  @ApiProperty({ example: '12.99' })
  @IsNotEmpty()
  precio: number
}

export class RespuestaCrearProductoDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
