import { Module } from '@nestjs/common'
import { ParametricasModule } from './parametricas/parametricas.module'
import { PedidosModule } from './pedidos/pedido.module'
import { ProductosModule } from './productos/productos.module'

@Module({
  imports: [ParametricasModule, ProductosModule, PedidosModule],
})
export class ApplicationModule {}
